*** Settings ***
Documentation    A test suit to test: Login to Cura
Resource    new.resource

Test Setup    L'utilisateur est sur la page d'accueil
Test Teardown    Fin de test et fermeture du navigateur

*** Test Cases ***
Login Test
    L'utilisateur souhaite prendre un rdv
    La page de connexion s'affiche
    L'utilisateur se connecte
    L'utilisateur est connecté à la page de rdv