*** Settings ***
Documentation    Un premier test d'accès à la page de login
Library    SeleniumLibrary

*** Variables ***
${url}    https://katalon-demo-cura.herokuapp.com
${browser}    firefox
${date}        02/07/2022
${comment}    Ceci est un commentaire

*** Test Cases ***
Login page test case

    Open Browser        ${url}    ${browser}
    Maximize Browser Window
    
    Wait Until Element Is Visible    id:btn-make-appointment
    Click Element    id:btn-make-appointment
    Wait Until Element Is Visible    id:btn-login
    Input Text    id:txt-username    John Doe
    Input Text    txt-password    ThisIsNotAPassword
    Click Element    id:btn-login

    Wait Until Element Is Visible    id:btn-book-appointment
    Click Element    id:combo_facility    
    Click Element    xpath://option[@value='Hongkong CURA Healthcare Center']
    Click Element    id:chk_hospotal_readmission
    Checkbox Should Be Selected    id:chk_hospotal_readmission
    Click Element    id:radio_program_medicaid
    Radio Button Should Be Set To    programs    Medicaid
    Input Text    id:txt_visit_date    ${date}
    Input Text    id:txt_comment    ${comment}
    Click Element    id:btn-book-appointment
    Click Element    xpath://a[@href='https://katalon-demo-cura.herokuapp.com/']
    
    Wait Until Element Is Visible    id:btn-make-appointment

    Close Browser

