*** Settings ***
Documentation    Un premier test d'accès à la page de login
Library    SeleniumLibrary
Library    RPA.Desktop

*** Variables ***
${url}    https://katalon-demo-cura.herokuapp.com
${browser}    firefox
${date}        02/07/2022
${comment}    Ceci est un commentaire

*** Keywords ***
Open Browser to Login Page
    Open Browser        ${url}    ${browser}
    Maximize Browser Window
    Click Element    id:btn-make-appointment

Login to Book Appointment
    Input Text    id:txt-username    John Doe
    Input Text    txt-password    ThisIsNotAPassword
    Click Element    id:btn-login

Book Appointment
    Click Element    id:combo_facility    
    Click Element    xpath://option[@value='Hongkong CURA Healthcare Center']
    Click Element    id:chk_hospotal_readmission
    Click Element    id:radio_program_medicaid
    Input Text    id:txt_visit_date    ${date}
    Input Text    id:txt_comment    ${comment}
    Click Element    id:btn-book-appointment
    Click Element    xpath://a[@href='https://katalon-demo-cura.herokuapp.com/']

End of Test and Close Browser   
    Close Browser


*** Test Cases ***
Login page test case
    Open Browser to Login Page
    Login to Book Appointment
    End of Test and Close Browser

Book Appointment Test
    Open Browser to Login Page
    Login to Book Appointment
    Book Appointment
    End of Test and Close Browser